const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');

const app = express();

app.use(express.json({type: '*/*'}));

const filesRouter = require('./routes/files');

app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: true,
    msg: "HTTP {{req.method}} {{req.url}}",
    expressFormat: true,
    colorize: false,
    ignoreRoute: function (req, res) {
        return false;
    }
}));

app.use('/api/files', filesRouter);

app.listen(8080);
