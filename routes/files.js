const express = require('express');
const fs = require('fs');
const router = express.Router();

const filesPath = './files';
const validFilenamesRegex = /^[\w\-. ]+.(log|txt|json|yaml|xml|js)$/;

router.route('/')
    .get(getAllFiles, (req, res) => {
        res.json({
            'message': 'Success',
            'files': req.filesList
        });
    })
    .post(checkBodyForCreation, checkBodyType, checkFilename, checkFileExistenceForCreation, (req, res) => {
        const body = req.body;

        fs.writeFile(`${filesPath}/${body.filename}`, body.content, function (error) {
            if (error) {
                res.status(500).json({
                    'message': 'Server error'
                });
            } else {
                res.json({
                    'message': 'File created successfully'
                });
            }
        });
    });

router.route('/:filename')
    .get(checkFileExistenceForInspection, (req, res) => {
        const filePath = `${filesPath}/${req.params.filename}`;

        fs.readFile(filePath, 'utf8', (error, data) => {
            if (error) {
                res.status(500).json({
                    'message': 'Server error'
                });
            }

            res.json({
                'message': 'Success',
                'filename': req.params.filename,
                'content': data,
                'extension': req.params.filename.split('.').slice(-1)[0],
                'uploadedDate': fs.statSync(filesPath).birthtime
            });
        });
    })
    .put(checkFileExistenceForInspection, checkBodyForModification, (req, res) => {
        fs.writeFile(`${filesPath}/${req.params.filename}`, req.body.content, (error) => {
            if (error) {
                res.status(500).json({
                    'message': 'Server error'
                });
            }

            res.json({
                'message': 'File changed successfully'
            });
        });
    })
    .delete(checkFileExistenceForInspection, (req, res) => {
        fs.unlink(`${filesPath}/${req.params.filename}`, (error) => {
            if (error) {
                res.status(500).json({
                    'message': 'Server error'
                });
            }

            res.json({
                'message': 'File deleted successfully'
            });
        });
    });

router.param('filename', (req, res, next, filename) => {
    if (!filename.match(validFilenamesRegex)) {
        res.status(400).json({
            'message': 'Invalid filename'
        });
    } else {
        next();
    }
});

function checkFolderExistence() {
    try {
        if (!fs.existsSync(filesPath)) {
            fs.mkdirSync(filesPath);
        }
    } catch (error) {
        throw error;
    }
}

function getAllFiles(req, res, next) {
    try {
        checkFolderExistence();
        req.filesList = fs.readdirSync(filesPath);
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Server error'
        });
    }
}

function checkBodyForCreation(req, res, next) {
    const body = req.body;
    console.log(body);

    if (!body.hasOwnProperty('filename')) {
        res.status(400).json({
            'message': "Please specify 'filename' parameter"
        });
    } else if (!body.hasOwnProperty('content')) {
        res.status(400).json({
            'message': "Please specify 'content' parameter"
        });
    } else {
        next();
    }
}

function checkBodyForModification(req, res, next) {
    const body = req.body;

    if (!body.hasOwnProperty('content')) {
        res.status(400).json({
            'message': "Please specify 'content' parameter"
        });
    } else if (typeof req.body.content !== 'string') {
        res.status(400).json({
            'message': 'Content is not of string type'
        });
    } else {
        next();
    }
}

function checkBodyType(req, res, next) {
    if (typeof req.body.filename !== 'string') {
        res.status(400).json({
            'message': 'Filename is not of string type'
        });
    } else if (typeof req.body.content !== 'string') {
        res.status(400).json({
            'message': 'Content is not of string type'
        });
    } else {
        next();
    }
}

function checkFilename(req, res, next) {
    if (!req.body.filename.match(validFilenamesRegex)) {
        res.status(400).json({
            'message': 'Invalid filename'
        });
    } else {
        next();
    }
}

function checkFileExistenceForCreation(req, res, next) {
    try {
        checkFolderExistence();

        if (fs.readdirSync(filesPath).includes(req.body.filename)) {
            res.status(400).json({
                'message': 'File with such filename already exists'
            });
        } else {
            next();
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Server error'
        });
    }
}

function checkFileExistenceForInspection(req, res, next) {
    try {
        checkFolderExistence();

        if (!fs.readdirSync(filesPath).includes(req.params.filename)) {
            res.status(400).json({
                'message': `No file with '${req.params.filename}' filename found`
            });
        } else {
            next();
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Server error'
        });
    }
}

module.exports = router;
